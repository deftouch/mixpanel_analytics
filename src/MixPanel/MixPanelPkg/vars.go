package MixPanelPkg

import (
	"deftouch_utility/database/sql"
	"sync"
)

type Config struct {
	InsertCounter           int64
	UnmarshallIntCounter    int64
	UnmarshallStringCounter int64
	AlreadyInsertedCounter  int64
}

type mixPanelProperties struct {
	Event      string                 `json:"event"`
	Properties map[string]interface{} `json:"properties"`
}
type properties struct {
	Event            string `json:"event,omitempty"`
	Time             int    `json:"time,omitempty"`
	DistinctId       string `json:"distinct_id,omitempty"`
	AppVersionString string `json:"$app_version_string,omitempty"`
	City             string `json:"$city,omitempty"`
	Device           string `json:"$device,omitempty"`
	InsertId         string `json:"$insert_id,omitempty"`
	LibVersion       string `json:"$lib_version,omitempty"`
	Model            string `json:"$model,omitempty"`
	MpEndPoint       string `json:"$mp_api_endpoint,omitempty"`
	Os               string `json:"$os,omitempty"`
	OsVersion        string `json:"$os_version,omitempty"`
	Radio            string `json:"$radio,omitempty"`
	Region           string `json:"$region,omitempty"`
	ScreenDpi        int    `json:"$screen_dpi,omitempty"`
	ScreenHeight     int    `json:"$screen_height,omitempty"`
	ScreenWidth      int    `json:"$screen_width,omitempty"`
	Wifi             bool   `json:"$wifi,omitempty"`
	Arena            int    `json:"arena,omitempty"`
	BallsBowled      int    `json:"balls_bowled,omitempty"`
	BallsFaced       string `json:"balls_faced,omitempty"`
	BotStatus        int    `json:"bot_status,omitempty"`
	CorrectShotCount int    `json:"correct_shot_count,omitempty"`
	Ismaster         int    `json:"is_master,omitempty"`
	CountryCode      string `json:"mp_country_code,omitempty"`
	MpLib            string `json:"mp_lib,omitempty"`
	MpProcessingTime int    `json:"mp_processing_time_ms,omitempty"`
	RunsGiven        int    `json:"runs_given,omitempty"`
	RunsScored       int    `json:"runs_scored,omitempty"`
	WicketsLost      string `json:"wickets_lost,omitempty"`
	WicketsTaken     int    `json:"wickets_taken,omitempty"`
	WrongShotCount   int    `json:"wrong_shot_count,omitempty"`
	IsBatting        int    `json:"is_batting,omitempty"`
	OppositeArena    int    `json:"opp_arena,omitempty"`
	OppositeRid      string `json:"opp_rid,omitempty"`
	BrowseCount      int    `json:"browseCount,omitempty"`
	OverCompleted    int    `json:"overCompleted,omitempty"`
	WicketsLosts     int    `json:"wicketsLost,omitempty"`
	Win              int    `json:"win,omitempty"`
	UtmSource        string `json:"utm_source,omtempty"`
	FailCount4       int    `json:"fail_count_4,omitempty"`
	FailCount89      int    `json:"fail_count_8_9,omitempty"`
	FailCount5       int    `json:"fail_count_5,omitempty"`
	FailCount7       int    `json:"fail_count_7,omitempty"`
	Failcount2       int    `json:"fail_count_2,omitempty"`
	FailCount6       int    `json:"fail_count_6,omitempty"`
	SwipeCount       int    `json:"swipe_count,omitempty"`
	FailCount01      int    `json:"fail_count_0_1,omitempty"`
	FailCount3       int    `json:"fail_count_3,omitempty"`
	AnonId           string `json:"$anon_id,omitempty"`
	IdentifiedId     string `json:"$identified_id,omitempty"`
	Source           string `json:"source,omitempty"`
	IsAutoOpen       int    `json:"is_auto_open,omitempty"`
	ItemId           string `json:"item_id,omitempty"`
	FailCountShort   int    `json:"fail_count_short,omitempty"`
	FailCountLeft    int    `json:"fail_count_left,omitempty"`
	FailCountRight   int    `json:"fail_count_right,omitempty"`
	FailCountFull    int    `json:"fail_count_full,omitempty"`
	DateTime         string `json:"date_time,omitempty"`
}
type propertiesOption struct {
	Event            string `json:"event,omitempty"`
	Time             int    `json:"time,omitempty"`
	DistinctId       string `json:"distinct_id,omitempty"`
	AppVersionString string `json:"$app_version_string,omitempty"`
	City             string `json:"$city,omitempty"`
	Device           string `json:"$device,omitempty"`
	InsertId         string `json:"$insert_id,omitempty"`
	LibVersion       string `json:"$lib_version,omitempty"`
	Model            string `json:"$model,omitempty"`
	MpEndPoint       string `json:"$mp_api_endpoint,omitempty"`
	Os               string `json:"$os,omitempty"`
	OsVersion        string `json:"$os_version,omitempty"`
	Radio            string `json:"$radio,omitempty"`
	Region           string `json:"$region,omitempty"`
	ScreenDpi        int    `json:"$screen_dpi,omitempty"`
	ScreenHeight     int    `json:"$screen_height,omitempty"`
	ScreenWidth      int    `json:"$screen_width,omitempty"`
	Wifi             bool   `json:"$wifi,omitempty"`
	Arena            int    `json:"arena,omitempty"`
	BallsBowled      int    `json:"balls_bowled,omitempty"`
	BallsFaced       string `json:"balls_faced,omitempty"`
	BotStatus        int    `json:"bot_status,omitempty"`
	CorrectShotCount int    `json:"correct_shot_count,omitempty"`
	Ismaster         int    `json:"is_master,omitempty"`
	CountryCode      string `json:"mp_country_code,omitempty"`
	MpLib            string `json:"mp_lib,omitempty"`
	MpProcessingTime int    `json:"mp_processing_time_ms,omitempty"`
	RunsGiven        int    `json:"runs_given,omitempty"`
	RunsScored       int    `json:"runs_scored,omitempty"`
	WicketsLost      string `json:"wickets_lost,omitempty"`
	WicketsTaken     int    `json:"wickets_taken,omitempty"`
	WrongShotCount   int    `json:"wrong_shot_count,omitempty"`
	IsBatting        int    `json:"is_batting,omitempty"`
	OppositeArena    int    `json:"opp_arena,omitempty"`
	OppositeRid      string `json:"opp_rid,omitempty"`
	BrowseCount      int    `json:"browseCount,omitempty"`
	OverCompleted    int    `json:"overCompleted,omitempty"`
	WicketsLosts     int    `json:"wicketsLost,omitempty"`
	Win              int    `json:"win,omitempty"`
	UtmSource        string `json:"utm_source,omtempty"`
	FailCount4       int    `json:"fail_count_4,omitempty"`
	FailCount89      int    `json:"fail_count_8_9,omitempty"`
	FailCount5       int    `json:"fail_count_5,omitempty"`
	FailCount7       int    `json:"fail_count_7,omitempty"`
	Failcount2       int    `json:"fail_count_2,omitempty"`
	FailCount6       int    `json:"fail_count_6,omitempty"`
	SwipeCount       int    `json:"swipe_count,omitempty"`
	FailCount01      int    `json:"fail_count_0_1,omitempty"`
	FailCount3       int    `json:"fail_count_3,omitempty"`
	AnonId           string `json:"$anon_id,omitempty"`
	IdentifiedId     string `json:"$identified_id,omitempty"`
	Source           string `json:"source,omitempty"`
	IsAutoOpen       int    `json:"is_auto_open,omitempty"`
	ItemId           string `json:"item_id,omitempty"`
	FailCountShort   int    `json:"fail_count_short,omitempty"`
	FailCountLeft    int    `json:"fail_count_left,omitempty"`
	FailCountRight   int    `json:"fail_count_right,omitempty"`
	FailCountFull    int    `json:"fail_count_full,omitempty"`
	DateTime         string `json:"date_time,omitempty"`
}

// var keys = [...]string{"event", "time", "distinct_id", "$app_version_string", "$city", "$device", "$insert_id", "$lib_version", "$model", "$mp_api_endpoint", "$os_version", "$radio", "$region", "$screen_dpi", "$screen_height", "$screen_width", "$wifi", "arena", "balls_bowled", "balls_faced", "bot_status", "correct_shot_count", "is_master", "mp_country_code", "mp_lib", "mp_processing_time_ms", "runs_given", "runs_scored", "wickets_lost", "wickets_taken", "wrong_shot_count"}

const (
	Event                     = "event"
	Time                      = "time"
	DistinctId                = "distinct_id"
	AppVersionString          = "$app_version_string"
	City                      = "$city"
	Device                    = "$device"
	InsertId                  = "$insert_id"
	LibVersion                = "$lib_version"
	Model                     = "$model"
	MpEndPoint                = "$mp_api_endpoint"
	Os                        = "$os"
	OsVersion                 = "$os_version"
	Radio                     = "$radio"
	ScreenDpi                 = "$screen_dpi"
	ScreenHeight              = "$screen_height"
	ScreenWidth               = "$screen_width"
	Wifi                      = "$wifi"
	Arena                     = "arena"
	BallsBowled               = "balls_bowled"
	BallsFaced                = "balls_faced"
	BotStatus                 = "bot_status"
	CorrectShotCount          = "correct_shot_count"
	Ismaster                  = "is_master"
	CountryCode               = "mp_country_code"
	UserPropertieCountryCode  = "$country_code"
	MpLib                     = "mp_lib"
	MpProcessingTime          = "mp_processing_time_ms"
	RunsGiven                 = "runs_given"
	RunsScored                = "runs_scored"
	WicketsLost               = "wickets_lost"
	WicketsTaken              = "wickets_taken"
	WrongShotCount            = "wrong_shot_count"
	IsBatting                 = "is_batting"
	OppositeArena             = "opp_arena"
	OppositeRid               = "opp_rid"
	BrowseCount               = "browseCount"
	OverCompleted             = "overCompleted"
	WicketsLosts              = "wicketsLost"
	Win                       = "win"
	UtmSource                 = "utm_source"
	FailCount4                = "fail_count_4"
	FailCount89               = "fail_count_8_9"
	FailCount5                = "fail_count_5"
	FailCount7                = "fail_count_7"
	Failcount2                = "fail_count_2"
	FailCount6                = "fail_count_6"
	SwipeCount                = "swipe_count"
	FailCount01               = "fail_count_0_1"
	FailCount3                = "fail_count_3"
	AnonId                    = "$anon_id"
	IdentifiedId              = "$identified_id"
	Source                    = "source"
	IsAutoOpen                = "is_auto_open"
	ItemId                    = "item_id"
	FailCountShort            = "fail_count_short"
	FailCountLeft             = "fail_count_left"
	FailCountRight            = "fail_count_right"
	FailCountFull             = "fail_count_full"
	DateTime                  = "date_time"
	TotalCharctersUnlocked    = "total_characters_unlocked"
	CurrentArenaLevel         = "current_arena_level"
	CurrentFbConnected        = "current_fb_connected"
	CurrentIapCurrency        = "current_iap_currency"
	CurrentResourceCoin       = "current_resource_coin"
	CurrentTrophyCount        = "current_trophy_count"
	InstallTime               = "installTime"
	TotalCardsCollected       = "total_cards_collected"
	TotalChestClaimed         = "total_chest_claimed"
	LastSeen                  = "$last_seen"
	AndroidAppVersion         = "$android_app_version"
	AndroidModel              = "$android_model"
	AndroidOsVersion          = "$android_os_version"
	HighestPurchaseHistory    = "highest_purchase_price"
	TotalMatchesWon           = "total_matches_won"
	TotalRematchesStarted     = "total_rematches_started"
	AcquistionSource          = "acquisition_source"
	CurrentXpLevel            = "current_xp_level"
	DeviceId                  = "deviceId"
	Rid                       = "rid"
	TotalCardsUpgradeEvents   = "total_cards_upgrade_events"
	TotalFriendMatchesStarted = "total_friend_matches_started"
	TotalMatchesStarted       = "total_matches_started"
	AndroidOs                 = "$android_os"
	OnBoardingSkipped         = "onboarding_skipped"
	Region                    = "$region"
	CurrentFbFriends          = "current_fb_friends"
	CurrentResourceGem        = "current_resource_gem"
	TotalIapCount             = "total_iap_count"
	UserName                  = "username"
	GeoSource                 = "$geo_source"
	CurrentFriendsCount       = "current_friends_count"
	Email                     = "email"
	HighestArenaLevel         = "highest_arena_level"
	TotalDaysSinceInstall     = "total_days_since_install"
	TotalFriendsAdded         = "total_friends_added"
	TotalIapSpend             = "total_iap_spend"
	AveragePurchaseValue      = "avg_purchase_price"
	HighestTrophyCount        = "highest_trophy_count"
	TotalMatchesLost          = "total_matches_lost"
	AndroidLibVersion         = "$android_lib_version"
	TimeZone                  = "$timezone"
	TotalSessionsStarted      = "total_sessions_started"
)

type UserProperties struct {
	TotalCharctersUnlocked    int     `json:"total_characters_unlocked,omitempty"`
	CurrentArenaLevel         int     `json:"current_arena_level,omitempty"`
	CurrentFbConnected        bool    `json:"current_fb_connected,omitempty"`
	CurrentIapCurrency        string  `json:"current_iap_currency,omitempty"`
	CurrentResourceCoin       int     `json:"current_resource_coin,omitempty"`
	CurrentTrophyCount        int     `json:"current_trophy_count,omitempty"`
	InstallTime               int     `json:"installTime,omitempty"`
	TotalCardsCollected       int     `json:"total_cards_collected,omitempty"`
	TotalChestClaimed         int     `json:"total_chest_claimed,omitempty"`
	LastSeen                  string  `json:"$last_seen,omitempty"`
	AndroidAppVersion         string  `json:"$android_app_version,omitempty"`
	AndroidModel              string  `json:"$android_model,omitempty"`
	AndroidOsVersion          string  `json:"$android_os_version,omitempty"`
	HighestPurchaseHistory    int     `json:"highest_purchase_price,omitempty"`
	TotalMatchesWon           int     `json:"total_matches_won,omitempty"`
	TotalRematchesStarted     int     `json:"total_rematches_started,omitempty"`
	AcquistionSource          string  `json:"acquisition_source,omitempty"`
	CurrentXpLevel            int     `json:"current_xp_level,omitempty"`
	DeviceId                  string  `json:"deviceId,omitempty"`
	Rid                       int     `json:"rid,omitempty"`
	TotalCardsUpgradeEvents   int     `json:"total_cards_upgrade_events,omitempty"`
	TotalFriendMatchesStarted int     `json:"total_friend_matches_started,omitempty"`
	TotalMatchesStarted       int     `json:"total_matches_started,omitempty"`
	AndroidOs                 string  `json:"$android_os,omitempty"`
	OnBoardingSkipped         bool    `json:"onboarding_skipped,omitempty"`
	Region                    string  `json:"$region,omitempty"`
	CurrentFbFriends          int     `json:"current_fb_friends,omitempty"`
	CurrentResourceGem        int     `json:"current_resource_gem,omitempty"`
	TotalIapCount             int     `json:"total_iap_count,omitempty"`
	UserName                  string  `json:"username,omitempty"`
	GeoSource                 string  `json:"$geo_source,omitempty"`
	CurrentFriendsCount       int     `json:"current_friends_count,omitempty"`
	Email                     string  `json:"email,omitempty"`
	HighestArenaLevel         int     `json:"highest_arena_level,omitempty"`
	TotalDaysSinceInstall     int     `json:"total_days_since_install,omitempty"`
	TotalFriendsAdded         int     `json:"total_friends_added,omitempty"`
	TotalIapSpend             int     `json:"total_iap_spend,omitempty"`
	City                      string  `json:"$city,omitempty"`
	CountryCode               string  `json:"$country_code,omitempty"`
	AveragePurchaseValue      float64 `json:"avg_purchase_price,omitempty"`
	HighestTrophyCount        int     `json:"highest_trophy_count,omitempty"`
	TotalMatchesLost          int     `json:"total_matches_lost,omitempty"`
	AndroidLibVersion         string  `json:"$android_lib_version,omitempty"`
	TimeZone                  string  `json:"$timezone,omitempty"`
	TotalSessionsStarted      int     `json:"total_sessions_started,omitempty"`
}

var AllFields []string = []string{
	"total_characters_unlocked",
	"current_arena_level",
	"current_fb_connected",
	"current_iap_currency",
	"current_resource_coin",
	"current_trophy_count",
	"installTime",
	"total_cards_collected",
	"total_chest_claimed",
	"$last_seen",
	"$android_app_version",
	"$android_model",
	"$android_os_version",
	"highest_purchase_price",
	"total_matches_won",
	"total_rematches_started",
	"acquisition_source",
	"current_xp_level",
	"deviceId",
	"rid",
	"total_cards_upgrade_events",
	"total_friend_matches_started",
	"total_matches_started",
	"$android_os",
	"onboarding_skipped",
	"$region",
	"current_fb_friends",
	"current_resource_gem",
	"total_iap_count",
	"username",
	"$geo_source",
	"current_friends_count",
	"email",
	"highest_arena_level",
	"total_days_since_install",
	"total_friends_added",
	"total_iap_spend",
	"$city",
	"$country_code",
	"avg_purchase_price",
	"highest_trophy_count",
	"total_matches_lost",
	"$android_lib_version",
	"$timezone",
	"total_sessions_started",
}

var (
	MasterDb sql.Database
	keys     []string
	wg       sync.WaitGroup
)

package MixPanelPkg

import (
	"deftouch_utility/DeftLog"
	"deftouch_utility/database/Schema/DeftQuery"
	"deftouch_utility/database/Schema/Field/Analytics"
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/vizzlo/mixpanel"
)

func (obj *Config) UpdateUserProperties() {
	// arr1 := []string{"total_characters_unlocked", "current_arena_level", "current_fb_connected", "current_iap_currency", "current_resource_coin", "current_trophy_count", "installTime", "total_cards_collected", "total_chest_claimed"}
	// var arr2 map[string]interface{}
	var err error
	// var properties Properties
	var pId []string
	var unpacked []string

	// token := "6fd3332dcb94302549cf5204b7afba22"
	// mp := mixpanel.New(token)

	// err = mp.Track("ea44841ec3e225d551f165d7089ba08d", "deviceid", map[string]interface{}{})

	apiSecret := "f95f5735efdfb4e96b6db1ce1ec84098"
	// API Secret is used to access the data export API
	client := mixpanel.NewExportClient(apiSecret)

	// Downloads all profiles that have been seen during the last hour
	profiles, err1 := client.ListProfiles(&mixpanel.ProfileQuery{
		LastSeenAfter: time.Now().Add(-time.Hour),
	})
	m2 := []map[string]interface{}{}

	for _, p := range profiles {
		m1 := p.Properties
		m2 = append(m2, m1)
		pId = append(pId, p.ID)
	}
	TotalUserProperties := []UserProperties{}
	for i, v := range m2 {
		properties := UserProperties{}
		stringified, err := json.Marshal(v)
		if err != nil {
			fmt.Println("error :", err)
		}
		unpacked = append(unpacked, string(stringified[:]))
		err = json.Unmarshal([]byte(unpacked[i]), &properties)
		if err != nil {
			fmt.Println("error :", err)
		}
		if pId[0] == properties.DeviceId {
			fmt.Println("properties.DeviceId", properties.DeviceId)

		}
		TotalUserProperties = append(TotalUserProperties, properties)
	}
	fmt.Println("length=", len(TotalUserProperties))

	for i := range TotalUserProperties {
		obj.updateAnalytics(TotalUserProperties[i])
	}
	fmt.Println(err, err1)

}
func (obj *Config) updateAnalytics(properties UserProperties) (userProperties UserProperties, isExist bool) {
	// var err error
	var valueArray []interface{}
	var fieldArray []string
	var ridFromDataBase int
	if properties.TotalCharctersUnlocked != -1 {
		fieldArray = append(fieldArray, TotalCharctersUnlocked)
		valueArray = append(valueArray, properties.TotalCharctersUnlocked)
	}
	if properties.CurrentArenaLevel != -1 {
		fieldArray = append(fieldArray, CurrentArenaLevel)
		valueArray = append(valueArray, properties.CurrentArenaLevel)
	}
	if properties.CurrentFbConnected == false {
		fieldArray = append(fieldArray, CurrentFbConnected)
		valueArray = append(valueArray, properties.CurrentFbConnected)
	} else {
		fieldArray = append(fieldArray, CurrentFbConnected)
		valueArray = append(valueArray, properties.CurrentFbConnected)
	}
	if properties.CurrentIapCurrency != "" {
		fieldArray = append(fieldArray, CurrentIapCurrency)
		valueArray = append(valueArray, properties.CurrentIapCurrency)
	}
	if properties.CurrentResourceCoin != -1 {
		fieldArray = append(fieldArray, CurrentResourceCoin)
		valueArray = append(valueArray, properties.CurrentResourceCoin)
	}
	if properties.CurrentTrophyCount != -1 {
		fieldArray = append(fieldArray, CurrentTrophyCount)
		valueArray = append(valueArray, properties.CurrentTrophyCount)
	}
	if properties.InstallTime != -1 {
		fieldArray = append(fieldArray, InstallTime)
		valueArray = append(valueArray, properties.InstallTime)
	}
	if properties.TotalCardsCollected != -1 {
		fieldArray = append(fieldArray, TotalCardsCollected)
		valueArray = append(valueArray, properties.TotalCardsCollected)
	}
	if properties.TotalChestClaimed != -1 {
		fieldArray = append(fieldArray, TotalChestClaimed)
		valueArray = append(valueArray, properties.TotalChestClaimed)
	}
	if properties.LastSeen != "" {
		fieldArray = append(fieldArray, LastSeen)
		valueArray = append(valueArray, properties.LastSeen)
	}
	if properties.AndroidAppVersion != "" {
		fieldArray = append(fieldArray, AndroidAppVersion)
		valueArray = append(valueArray, properties.AndroidAppVersion)
	}
	if properties.AndroidModel != "" {
		fieldArray = append(fieldArray, AndroidModel)
		valueArray = append(valueArray, properties.AndroidModel)
	}
	if properties.AndroidOsVersion != "" {
		fieldArray = append(fieldArray, AndroidOsVersion)
		valueArray = append(valueArray, properties.AndroidOsVersion)
	}
	if properties.HighestPurchaseHistory != -1 {
		fieldArray = append(fieldArray, HighestPurchaseHistory)
		valueArray = append(valueArray, properties.HighestPurchaseHistory)
	}
	if properties.TotalMatchesWon != -1 {
		fieldArray = append(fieldArray, TotalMatchesWon)
		valueArray = append(valueArray, properties.TotalMatchesWon)
	}
	if properties.TotalRematchesStarted != -1 {
		fieldArray = append(fieldArray, TotalRematchesStarted)
		valueArray = append(valueArray, properties.TotalRematchesStarted)
	}
	if properties.AcquistionSource != "" {
		fieldArray = append(fieldArray, AcquistionSource)
		valueArray = append(valueArray, properties.AcquistionSource)
	}
	if properties.CurrentXpLevel != -1 {
		fieldArray = append(fieldArray, CurrentXpLevel)
		valueArray = append(valueArray, properties.CurrentXpLevel)
	}
	if properties.Rid != -1 {
		fieldArray = append(fieldArray, Rid)
		valueArray = append(valueArray, properties.Rid)
	}
	if properties.TotalCardsUpgradeEvents != -1 {
		fieldArray = append(fieldArray, TotalCardsUpgradeEvents)
		valueArray = append(valueArray, properties.TotalCardsUpgradeEvents)
	}
	if properties.TotalFriendMatchesStarted != -1 {
		fieldArray = append(fieldArray, TotalFriendMatchesStarted)
		valueArray = append(valueArray, properties.TotalFriendMatchesStarted)
	}
	if properties.TotalMatchesStarted != -1 {
		fieldArray = append(fieldArray, TotalMatchesStarted)
		valueArray = append(valueArray, properties.TotalMatchesStarted)
	}
	if properties.AndroidOs != "" {
		fieldArray = append(fieldArray, AndroidOs)
		valueArray = append(valueArray, properties.AndroidOs)
	}
	if properties.OnBoardingSkipped != false {
		fieldArray = append(fieldArray, OnBoardingSkipped)
		valueArray = append(valueArray, properties.OnBoardingSkipped)
	} else {
		fieldArray = append(fieldArray, OnBoardingSkipped)
		valueArray = append(valueArray, properties.OnBoardingSkipped)
	}
	if properties.Region != "" {
		fieldArray = append(fieldArray, Region)
		valueArray = append(valueArray, properties.Region)
	}
	if properties.CurrentFbFriends != -1 {
		fieldArray = append(fieldArray, CurrentFbFriends)
		valueArray = append(valueArray, properties.CurrentFbFriends)
	}
	if properties.CurrentResourceGem != -1 {
		fieldArray = append(fieldArray, CurrentResourceGem)
		valueArray = append(valueArray, properties.CurrentResourceGem)
	}
	if properties.TotalIapCount != -1 {
		fieldArray = append(fieldArray, TotalIapCount)
		valueArray = append(valueArray, properties.TotalIapCount)
	}
	if properties.UserName != "" {
		fieldArray = append(fieldArray, UserName)
		valueArray = append(valueArray, properties.UserName)
	}
	if properties.GeoSource != "" {
		fieldArray = append(fieldArray, GeoSource)
		valueArray = append(valueArray, properties.GeoSource)
	}
	if properties.CurrentFriendsCount != -1 {
		fieldArray = append(fieldArray, CurrentFriendsCount)
		valueArray = append(valueArray, properties.CurrentFriendsCount)
	}
	if properties.Email != "" {
		fieldArray = append(fieldArray, Email)
		valueArray = append(valueArray, properties.Email)
	}
	if properties.HighestArenaLevel != -1 {
		fieldArray = append(fieldArray, HighestArenaLevel)
		valueArray = append(valueArray, properties.HighestArenaLevel)
	}
	if properties.TotalDaysSinceInstall != -1 {
		fieldArray = append(fieldArray, TotalDaysSinceInstall)
		valueArray = append(valueArray, properties.TotalDaysSinceInstall)
	}
	if properties.TotalFriendsAdded != -1 {
		fieldArray = append(fieldArray, TotalFriendsAdded)
		valueArray = append(valueArray, properties.TotalFriendsAdded)
	}
	if properties.TotalIapSpend != -1 {
		fieldArray = append(fieldArray, TotalIapSpend)
		valueArray = append(valueArray, properties.TotalIapSpend)
	}
	if properties.City != "" {
		fieldArray = append(fieldArray, City)
		valueArray = append(valueArray, properties.City)
	}
	if properties.CountryCode != "" {
		fieldArray = append(fieldArray, UserPropertieCountryCode)
		valueArray = append(valueArray, properties.CountryCode)
	}
	if properties.AveragePurchaseValue != -1 {
		fieldArray = append(fieldArray, AveragePurchaseValue)
		valueArray = append(valueArray, properties.AveragePurchaseValue)
	}
	if properties.HighestTrophyCount != -1 {
		fieldArray = append(fieldArray, HighestTrophyCount)
		valueArray = append(valueArray, properties.HighestTrophyCount)
	}
	if properties.TotalMatchesLost != -1 {
		fieldArray = append(fieldArray, TotalMatchesLost)
		valueArray = append(valueArray, properties.TotalMatchesLost)
	}
	if properties.AndroidLibVersion != "" {
		fieldArray = append(fieldArray, AndroidLibVersion)
		valueArray = append(valueArray, properties.AndroidLibVersion)
	}
	if properties.TimeZone != "" {
		fieldArray = append(fieldArray, TimeZone)
		valueArray = append(valueArray, properties.TimeZone)
	}
	if properties.TotalSessionsStarted != -1 {
		fieldArray = append(fieldArray, TotalSessionsStarted)
		valueArray = append(valueArray, properties.TotalSessionsStarted)
	}

	if properties.DeviceId != "" {
		fieldArray = append(fieldArray, DeviceId)
		valueArray = append(valueArray, properties.DeviceId)
	} else {
		return
	}
	isExist, ridFromDataBase = obj.checkingDeviceId(properties.DeviceId)

	DeftLog.Print("analytics, UpdateAnalytics, getUserDetails=", isExist)
	DeftLog.Print("analytics, UpdateAnalytics, FieldArray=", fieldArray)

	DeftLog.Print("analytics, UpdateAnalytics, valuearray", valueArray)
	if !isExist {
		_ = obj.insertAnalyticsToUserPorperties(fieldArray, valueArray)
	} else if isExist && ridFromDataBase == 0 {
		valueArray = append(valueArray, properties.DeviceId)
		_ = obj.updateAnalyticsToUserProperties(fieldArray, valueArray, properties.DeviceId)
	} else if isExist && properties.Rid != ridFromDataBase {
		if properties.Rid != 0 {
			_ = obj.insertAnalyticsToUserPorperties(fieldArray, valueArray)
		} else {
			DeftLog.Error("DeviceID is Exist and Rid from database is exist but from the client rid is blank")
			return
		}
	} else if isExist && ridFromDataBase == properties.Rid {
		valueArray = append(valueArray, properties.DeviceId)
		_ = obj.updateAnalyticsToUserProperties(fieldArray, valueArray, properties.DeviceId)
	} else {
		DeftLog.Log("UpdateAnalytics Else Part")
	}
	userProperties = properties
	return

}
func (obj *Config) insertAnalyticsToUserPorperties(fieldArray []string, valueArray []interface{}) (err error) {

	DeftLog.Print("len field", len(fieldArray), "valuearray", len(valueArray))

	dQuery := DeftQuery.CreateQuery().
		SetTable("datastore").
		Insert(fieldArray...).
		SetValues(valueArray...).Build()
	dQuery = MasterDb.RomExec(dQuery)
	err = dQuery.GetError()
	if err != nil {
		DeftLog.Err(err, "analytics, InsertAnalyticsToDb, error while inserting data into UserProperties table")
		return
	} else {
		DeftLog.Print("Data inserted into insertDataIntoAnalytics ")
	}

	return
}
func (obj *Config) updateAnalyticsToUserProperties(fieldArray []string, valueArray []interface{}, deviceId string) (err error) {

	dQuery := DeftQuery.CreateQuery().
		SetTable("datastore").
		Update(fieldArray...).
		Where(Analytics.Deviceid).SetValues(valueArray...).Build()
	dQuery = MasterDb.RomExec(dQuery)
	err = dQuery.GetError()
	if err != nil {
		DeftLog.Err(err, "analytics, UpdateAnalyticsToDb, error while updating data into UserProperties table")
		return
	}
	DeftLog.Print("analytics, UpdateAnalyticsToDb, UpdateRowAffected", dQuery.UpdateRowAffected)
	return
}
func (obj *Config) checkingDeviceId(deviceId string) (isExist bool, rid int) {
	dQuery := DeftQuery.CreateQuery().
		SetTable("datastore").
		Select(DeviceId, Rid).
		Where(DeviceId).SetValues(deviceId).Build()

	dQuery = MasterDb.RomQuery(dQuery)
	err := dQuery.GetError()
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println("Database device is not in database ")
	}
	isExist = dQuery.IsEntryExist()
	data := dQuery.GetData()
	if !isExist {
		rid = 0
		return
	} else if isExist && data[0][1] == "" {
		rid = 0
		return
	} else {
		content := dQuery.GetFirstContentData()
		ridString := obj.checkNilData(content[Rid])
		rid, err = strconv.Atoi(ridString)
		if err != nil {
			log.Fatal(err)
		} else {
			fmt.Println("Not able to Convert Rid ")
		}
	}

	return

}
func (obj *Config) checkNilData(content *DeftQuery.Content) string {
	if content.IsDataNill() {
		return "0"
	}
	return content.GetData()
}

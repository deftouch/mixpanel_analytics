package main

import (
	"MixPanel/MixPanelPkg"
	"deftouch_utility/DeftLog"
	"deftouch_utility/DeftouchTeams"
	"deftouch_utility/constant/NotificationTitle"
	"deftouch_utility/constant/ServiceStatus"
	"fmt"
	"os"
	"time"
)

var db MixPanelPkg.Config

func main() {
	ScheduleJob()
}
func ScheduleJob() {
	db.GetDbConnectionNew()
	for {
		fmt.Println("MixPanel Started")
		go DeftouchTeams.SendNotifyServiceToTeam("MixPanel", nil, "MixPanel", 1, NotificationTitle.SERVICE_STARTED, ServiceStatus.HEALTHY, "MixPanel")
		db.UpdateUserProperties()
		db.UpdateMixPanelEvents()

		go DeftouchTeams.SendNotifyServiceToTeam("MixPanel", nil, "MixPanel", 1, "Job Completed", ServiceStatus.HEALTHY, "MixPanel")
		DeftLog.Printf("InsertCounter", db.InsertCounter, "AlreadyInsertedCounter", db.AlreadyInsertedCounter, "UnmarshallIntCounter", db.UnmarshallIntCounter, "UnmarshallStringCounter", db.UnmarshallStringCounter)

		fmt.Println("MixPanel Done")
		os.Exit(0)
		time.Sleep(time.Hour * 12)
	}
}

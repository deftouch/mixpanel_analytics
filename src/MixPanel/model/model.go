package model

type Database struct {
	IP       string `json:"Ip,omitempty"`
	Password string `json:"Password,omitempty"`
	Port     string `json:"Port,omitempty"`
	Type     string `json:"Type,omitempty"`
	Username string `json:"UserName,omitempty"`
	Schema   string `json:"Schema,omitempty"`
}

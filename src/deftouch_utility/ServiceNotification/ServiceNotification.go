package ServiceNotification

type ServiceNotificationModel struct {
	ProjectType       string `json:"ProjectType"`
	SenderServiceName string `json:"SenderServiceName"`
	ServiceName       string `json:"ServiceName"`
	ServiceId         string `json:"ServiceId"`
	Error             string `json:"Error"`
	SenderPodnName    string `json:"SenderPodName"`
	ServiceStatus     string `json:"ServiceStatus"`
}

package DeftLog

import (
	"deftouch_utility/constant/ProjectType"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

var projectType string

// setup would normally be an init() function, however, there seems
// to be something awry with the testing framework when we set the
// global Logger from an init()
func setup() {
	// UNIX Time is faster and smaller than most timestamps
	// If you set zerolog.TimeFieldFormat to an empty string,
	// logs will write with UNIX time
	//zerolog.TimeFieldFormat = ""
	// // In order to always output a static time to stdout for these
	// // examples to pass, we need to override zerolog.TimestampFunc
	// // and log.Logger globals -- you would not normally need to do this
	// zerolog.TimestampFunc = func() time.Time {
	// 	return time.Date(2008, 1, 8, 17, 5, 05, 0, time.UTC)
	// }
	// log.Logger = zerolog.New(os.Stdout).With().Timestamp().Logger()
}
func Init(_projectType string) {
	projectType = _projectType
	if projectType == ProjectType.PRODUCTION || projectType == ProjectType.BACKUP_PRODUCTION {
		zerolog.TimeFieldFormat = ""
	}
	setup()
	return
}

func Printf(v ...interface{}) {
	if isNotProduction() {
		log.Print(v...)
	}
	return
}

func Print(v ...interface{}) {
	if isNotProduction() {
		log.Print(v...)
	}
}
func PPrintf(v ...interface{}) {
	log.Print(v...)
}

func PPrint(v ...interface{}) {
	log.Print(v...)
}
func PLog(v string) {
	log.Log().Msg(v)
}
func PInfo(msg string) {
	log.Info().Msg(msg)
}
func Log(v string) {
	if isNotProduction() {
		log.Log().Msg(v)
	}
}
func isNotProduction() bool {
	return (projectType != ProjectType.PRODUCTION && projectType != ProjectType.BACKUP_PRODUCTION)
}
func Info(msg string) {
	if isNotProduction() {
		log.Info().Msg(msg)
	}
}
func Warn(msg string) {
	log.Warn().Msg(msg)
}

func Error(msg string) {
	log.Error().Msg(msg)
}

func Err(err error, msg string) {
	log.Err(err).Msg(msg)
}

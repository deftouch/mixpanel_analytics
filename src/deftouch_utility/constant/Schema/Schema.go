package Schema

//Schema Info
const (
	Auth_Table                  = "auth"
	Player_Info_Table           = "player_info"
	Player_Character_Info_table = "player_character_info"
	Player_Stats_table          = "player_stats"
	Friend_List_Table           = "friend_list"

	//Rsources Table Name:Player_Info_Table
	Coins     = "coins"
	Gems      = "gems"
	Resources = "coins,gems"
	Name      = "name"
	Trophy    = "trophy"
)

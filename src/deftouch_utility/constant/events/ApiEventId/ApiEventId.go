package ApiEventId

const (
	//service Gateway
	MaintainenceInfo    int32 = 0 //done
	CurrentServertime   int32 = 1 //done
	ClientConfiguration int32 = 2 //done

	//Leaderboard //service Socialhub
	RequestLeaderboard       int32 = 3 //done
	RequestMiniLeaderboard   int32 = 4 //done
	RequestCheckAccountExist       = 5
	RequestAuth                    = 6
	RequstDataMigration            = 7
	RequestPlayerInfomation        = 8 //done
	RequestInventoryItems          = 9 // done

	//Chest related ,service Databse
	RequestChestSlot         int32 = 21 //done
	RequestChestReward       int32 = 22 //done
	RequestChestOpenTimer    int32 = 23 //done
	RequestChestOpen         int32 = 24 //done
	RequestSpecialChestOpen  int32 = 25 //done
	RequestSpeedUpChestTimer int32 = 26 //done

	//Social Features //service Socialhub
	RequestOneSignalId              int32 = 27 //done
	RequestUpdateOneSignalId        int32 = 28 //done
	RequestGetRefferalCode          int32 = 29 //done
	RequestVeryfyRefferralCode      int32 = 30 //done
	RequestGetRefferralCodeStats    int32 = 31 //done
	RequestUpdateRefferralCodeStats int32 = 32 //done
	//Friend mode //service Socialhub
	RequestAddFriend            int32 = 33 //done
	RequestRemoveFriend         int32 = 34 //done
	RequestGetFriendList        int32 = 35 //done
	RequestGetCurrentOnlineList int32 = 36 //done

	//Service to service request
	UpdateUserNameToRedisLeaderboard int32 = 37 //need to test
	UpdateRedisLeaderboard           int32 = 38 //need to test

	//Fb related //service Socialhub
	RequestFbInfoUpdate         int32 = 39 //done
	RequestFbIdUpdate           int32 = 40 //done
	RequestGetProfilePicture    int32 = 41 //done
	RequestupdateProfilePicture int32 = 42 //done

	//User Resources //service Database
	RequestCoins           int32 = 43 //done
	RequestGems            int32 = 44 //done
	RequestUpdateCoins     int32 = 45 //done
	RequestUpdateGems      int32 = 46 //done
	RequestResources       int32 = 47 //done
	RequestUpdateResources int32 = 48 //done
	//All above events are done
	//Usrer Information //services Database
	RequestName         int32 = 49 //done
	RequestTrophy       int32 = 50 // done
	RequestArenaIndex   int32 = 51 //@navven need to do
	RequestUpdateName   int32 = 52 //done
	RequestUpdateTrophy int32 = 53 //done

	RequestDailyActivity int32 = 54 //done
	RequestQuestInfo     int32 = 55 //done
	RequestPlayerXplevel int32 = 56 //@subrata need to do//serverside done

	RequestPrchasingItemInfo        int32 = 57 //@done
	RequestPrchasingItemShoes       int32 = 58 //@done
	RequestPrchasingItemEmblems     int32 = 59 //@done
	RequestPrchasingItemEmoji       int32 = 60 //@done
	RequestPrchasingItemCelebration int32 = 61 //@done
	RequestPrchasingItemJersey      int32 = 62 //@done

	RequestLimitWinPerDay            int32 = 63 //@navven need to do
	RequestHighestUnlockedArenaIndex int32 = 64 //@navven need to do
	RequestAccountCreationDate       int32 = 65 //@navven need to do
	RequestHighestTrophies           int32 = 66 //@navven need to do
	RequestMegaQuestClaimReward      int32 = 67 //@navven need to do
	RequestUserTeam                  int32 = 68 //@navven need to do
	RequestUserCity                  int32 = 69 //@navven need to do

	RequestUpdatePlayerXplevel     int32 = 70 //@subrata need to do//serverside done
	RequestUpdateUserTeam          int32 = 71 //@navven need to do
	RequestUpdateUserCity          int32 = 72 //@navven need to do
	RequestTeamConfiguration       int32 = 73 //@subrata need to do//serverside done
	RequestUpdateTeamConfiguration int32 = 74 //@subrata need to do//serverside done
	RequestPlayerStats             int32 = 75 //@subrata need to do//serverside done
	RequestUpdatePlayerStats       int32 = 76 //@subrata need serverside done
	RequestUnlockCharacters        int32 = 77 //@subrata need to do//serverside done
	RequestUpgradeUnlockCharacters int32 = 78 //@subrata need to do//serverside done
	RequestUpdateQuestInfo         int32 = 79 //@subrata need to do//serverside done
	RequestClaimQuestInfo          int32 = 80 //@subrata need to do//serverside done
	RequestClaimOverallQuestInfo   int32 = 81 //@subrata need to do//serverside done

	RequestAddCoins        int32 = 82 //done
	RequestAddGems         int32 = 83 //done
	RequestDeductCoins     int32 = 84 //done
	RequestDeductGems      int32 = 85 //done
	RequestDeductResources int32 = 86 //done

	RequestUpdateDailyActivity int32 = 87 //@subrata need to do//serverside done
	RequestAddPlayerXplevel    int32 = 88 //@subrata need to do//serverside done

	RequestBuyPurchasingItemInfo         int32 = 89  //@subrata need to do//serverside done
	RequestSelectPurchasingItemInfo      int32 = 90  //@subrata need to do//serverside done
	RequestPurchasingBundlePack          int32 = 91  //@subrata need to do//serverside done
	RequestIAPPurchase                   int32 = 92  //@subrata need to do//serverside done
	RequestAchivementBadgeLevel          int32 = 93  //@subrata need to do//serverside done
	RequestProfileDetails                int32 = 94  //@subrata need to do//serverside done
	RequestValidateResources             int32 = 95  //@subrata need to do//serverside done
	RequestGameStart                     int32 = 96  //@subrata need to do//serverside done
	RequestRegisterPlayWithFriend        int32 = 97  //@subrata need to do//serverside done
	RequestCheckPlayWithFriend           int32 = 98  //@subrata need to do//serverside done
	RequestSupperOverClaimReward         int32 = 99  //@subrata need to do//serverside done
	RequestUpdateCountry                 int32 = 100 //done
	RequestModifyTrophiesOnLocalMismatch int32 = 101 //done
	RequestGetUserTeamConf               int32 = 102 //done
	RequestModifyPlayerStats             int32 = 103 //done
	RequestDashboardOpen                 int32 = 104 //done

)

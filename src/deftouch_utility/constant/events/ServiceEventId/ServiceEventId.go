package ServiceEventId

const (
	Gateway       int32 = 1
	Auth          int32 = 2
	Configuration int32 = 3
	SocialHub     int32 = 4
	Database      int32 = 5
	Analytics     int32 = 6
)

package ServiceName

const (
	ConfigurationService = "ConfigurationService"
	SocialHubService     = "SocialHubService"
	DatabaseService      = "DatabaseService"
	GatewayService       = "GatewayService"
	AuthService          = "AuthService"
	NotificationService  = "NotificationService"
)

package ProjectType

const (
	DEVELOPMENT       = "Dev"
	STAGING           = "Staging"
	PRODUCTION        = "Production"
	BACKUP_PRODUCTION = "BackupProduction"
	BACKUP_STAGING    = "BackupStaging"
)

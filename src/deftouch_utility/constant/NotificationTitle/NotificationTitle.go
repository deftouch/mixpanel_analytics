package NotificationTitle

const (
	SERVICE_ERROR   = "Service Error"
	SERVICE_STARTED = "Service Started"
	SERVICE_EXIT    = "Service Exit"
)

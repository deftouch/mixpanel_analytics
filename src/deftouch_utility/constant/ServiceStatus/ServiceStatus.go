package ServiceStatus

const (
	HEALTHY  = "HEALTHY"
	DOWN     = "DOWN"
	MODERATE = "MODERATE"
)

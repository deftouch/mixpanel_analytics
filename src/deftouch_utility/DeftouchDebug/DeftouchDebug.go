package DeftouchDebug

import (
	"deftouch_utility/constant/ProjectType"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var Logger *zap.Logger
var projectType string

func Init(project_Type string) {
	projectType = project_Type
	switch projectType {
	case ProjectType.DEVELOPMENT:
		config := zap.NewDevelopmentConfig()
		config.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
		Logger, _ = config.Build()
		break
	case ProjectType.STAGING:
		Logger, _ = zap.NewDevelopment()
		break
	case ProjectType.PRODUCTION:
		Logger, _ = zap.NewProduction()
		break
	case ProjectType.BACKUP_PRODUCTION:
		Logger, _ = zap.NewProduction()
		break
	default:
		Logger, _ = zap.NewDevelopment()

	}
}

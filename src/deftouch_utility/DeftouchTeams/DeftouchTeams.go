package DeftouchTeams

import (
	"deftouch_utility/ServiceNotification"
	"deftouch_utility/constant/NotificationTitle"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	goteamsnotify "github.com/atc0005/go-teams-notify/v2"
)

var NotificationSendTime int64
var NextNotificationSendDuration int64 = 60

func SendNotifyServiceToTeam(ProjectType string, errorInfo error, serviceName string, ServiceID int32, Title string, Status string, SenderServiceName string) {
	log.Println("SendNotifyServiceToTeam ")
	if NotificationSendTime > time.Now().Unix() {
		return
	}
	NotificationSendTime = time.Now().Unix() + NextNotificationSendDuration
	var serviceNotificationModel ServiceNotification.ServiceNotificationModel
	if errorInfo != nil {
		serviceNotificationModel.Error = errorInfo.Error()
	} else {
		serviceNotificationModel.Error = "nil"
	}
	serviceNotificationModel.ServiceName = serviceName
	serviceNotificationModel.ServiceId = strconv.Itoa((int)(ServiceID))
	serviceNotificationModel.ProjectType = ProjectType
	serviceNotificationModel.SenderServiceName = SenderServiceName
	serviceNotificationModel.SenderPodnName = GetEnv("MY_POD_NAME")
	serviceNotificationModel.ServiceStatus = Status

	data, _ := json.Marshal(&serviceNotificationModel)
	dataString := string(data[:])
	fmt.Println("SendNotifyServiceToTeam", dataString)
	SendServiceStatusToTeams(serviceNotificationModel, Title)
	return
}
func SendServiceStatusToTeams(serviceNotificationModel ServiceNotification.ServiceNotificationModel, title string) error {
	// init the client
	mstClient := goteamsnotify.NewClient()

	// setup webhook url
	//webhookUrl := "https://outlook.office365.com//webhookb2/33fba4d8-1106-418d-8bec-771cb9bef399@f28dda4f-9e42-46f3-89b3-d72520445b0f/IncomingWebhook/7bfea279a9d54db2a18ddb2e76969bf8/d426e4ac-9ba9-4aea-ae5b-19f7cdd0de44"
	webhookUrl := "https://outlook.office365.com//webhookb2/7444a503-7a57-4da2-b93b-8d9ca95dd5b1@f28dda4f-9e42-46f3-89b3-d72520445b0f/IncomingWebhook/a702d2b3ecd04492a697a3792a980fd8/d426e4ac-9ba9-4aea-ae5b-19f7cdd0de44"

	// setup message card
	msgCard := goteamsnotify.NewMessageCard()
	msgCard.Title = serviceNotificationModel.ProjectType + " " + title
	msgCard.Text = " **ProjectType :**" + serviceNotificationModel.ProjectType + "<br>" + "**SenderServiceName :**" + serviceNotificationModel.SenderServiceName + "<br>" + "**SenderPodName :**" + serviceNotificationModel.SenderPodnName + "<br>" + "**ServiceId :**" + serviceNotificationModel.ServiceId + "<br>" + "**ServiceName :**" + serviceNotificationModel.ServiceName + "<br>" + "**Error :**" + serviceNotificationModel.Error + "<br>" + "**ServiceStatus :**" + serviceNotificationModel.ServiceStatus
	if title == NotificationTitle.SERVICE_ERROR {
		msgCard.ThemeColor = "#FF0000"
	} else if title == NotificationTitle.SERVICE_STARTED {
		msgCard.ThemeColor = "#00FF00"
	} else {
		msgCard.ThemeColor = "#F1C232"
	}

	// send
	err := mstClient.Send(webhookUrl, msgCard)
	if err != nil {
		fmt.Println("ERROR", err.Error())
	}
	return err
}
func GetEnv(key string) string {
	return os.Getenv(key)
}

//func SendTheMessage() error {
//	// init the client
//	mstClient := goteamsnotify.NewClient()
//	//ctx:=context.Background()
//	// setup webhook url
//	webhookUrl := "https://outlook.office365.com//webhookb2/33fba4d8-1106-418d-8bec-771cb9bef399@f28dda4f-9e42-46f3-89b3-d72520445b0f/IncomingWebhook/7bfea279a9d54db2a18ddb2e76969bf8/d426e4ac-9ba9-4aea-ae5b-19f7cdd0de44"
//	//https://deftouchinteractiveartpvtlt.webhook.office.com/webhookb2/33fba4d8-1106-418d-8bec-771cb9bef399@f28dda4f-9e42-46f3-89b3-d72520445b0f/IncomingWebhook/7bfea279a9d54db2a18ddb2e76969bf8/d426e4ac-9ba9-4aea-ae5b-19f7cdd0de44
//	//webhookUrl := "https://deftouchinteractiveartpvtlt.webhook.office.com/webhookb2/33fba4d8-1106-418d-8bec-771cb9bef399@f28dda4f-9e42-46f3-89b3-d72520445b0f/IncomingWebhook/bb2d0fbfeeb94c5ab035eec3599e8efb/d426e4ac-9ba9-4aea-ae5b-19f7cdd0de44"
//
//	// setup message card
//	msgCard := goteamsnotify.NewMessageCard()
//	msgCard.Title = "Hello world"
//	msgCard.Text = "Here are some examples of formatted stuff like " +
//		"<br> * this list itself  <br> * **bold** <br> * *italic* <br> * ***bolditalic***"
//	msgCard.ThemeColor = "#DF813D"
//
//	// send
//	err := mstClient.Send(webhookUrl, msgCard)
//	if err != nil {
//		fmt.Println("ERROR", err.Error())
//	}
//	return err
//	//err:= mstClient.SendWithRetry(ctx,webhookUrl,msgCard,5,2)
//	//if err!=nil{
//	//	fmt.Println("ERROR",err.Error())
//	//}
//	//return err
//}

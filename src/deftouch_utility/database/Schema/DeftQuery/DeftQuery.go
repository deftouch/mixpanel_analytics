package DeftQuery

import (
	"fmt"
	"strconv"
)

const (
	QueryType_Select = 0
	QueryType_Update = 1
	QueryType_Insert = 2
)

type DQuery struct {
	table             string
	fields            []string
	wheres            []string
	values            []interface{}
	isEntryExist      bool
	_error            error
	contentData       []map[string]*Content
	data              [][]string
	query             string
	queryType         int
	UpdateRowAffected int64
}

type Content struct {
	data   string
	isNill bool
}

func (deftquery *DQuery) SetIsEnryExist(status bool) {
	deftquery.isEntryExist = status
	return
}
func (deftquery *DQuery) SetError(err error) {
	deftquery._error = err
	return
}
func (deftquery *DQuery) SetData(data [][]string) {
	deftquery.data = data
	legthOfData := len(data)
	if legthOfData == 0 {
		return
	}
	deftquery.convertToMap(data, legthOfData)
	deftquery.SetIsEnryExist(true)
	return
}
func (deftquery *DQuery) GetFirstContentData() map[string]*Content {
	return deftquery.contentData[0]
}
func (deftquery *DQuery) convertToMap(data [][]string, legthOfData int) {
	deftquery.contentData = make([]map[string]*Content, legthOfData)
	lengthOfFields := len(deftquery.fields)
	for i := 0; i < legthOfData; i++ {
		contentMap := make(map[string]*Content, lengthOfFields)
		for j := 0; j < lengthOfFields; j++ {
			content := &Content{}
			content.data = data[i][j]
			if len(content.data) == 0 {
				content.isNill = true
			}
			contentMap[deftquery.fields[j]] = content
		}
		deftquery.contentData[0] = contentMap
	}
	return
}
func (c *Content) IsDataNill() bool {
	return c.isNill
}
func (deftquery *DQuery) IsExistInContent(index int, key string) (c *Content, ok bool) {
	c, ok = deftquery.contentData[index][key]
	return
}

func (c *Content) GetData() string {
	return c.data
}
func (c *Content) GetDataInt() (dataInt int) {
	dataInt, _ = strconv.Atoi(c.data)
	return
}
func (c *Content) GetDataFloat() (dataInt float64) {
	dataInt, _ = strconv.ParseFloat(c.data, 64)
	return
}
func (c *Content) GetDataInt64() (dataInt int64) {
	dataInt, _ = strconv.ParseInt(c.data, 10, 64)
	return
}
func (c *Content) GetDataInt32() (dataInt int32) {
	data, _ := strconv.Atoi(c.data)
	dataInt = int32(data)
	return
}
func CreateQuery() (deftquery *DQuery) {
	deftquery = &DQuery{}
	return deftquery
}
func (deftquery *DQuery) GetQuery() string {
	return deftquery.query
}
func (deftquery *DQuery) GetQueryType() int {
	return deftquery.queryType
}
func (deftquery *DQuery) GetValues() []interface{} {
	return deftquery.values
}
func (deftquery *DQuery) IsEntryExist() bool {
	return deftquery.isEntryExist
}
func (deftquery *DQuery) GetError() error {
	return deftquery._error
}
func (deftquery *DQuery) GetContentData() []map[string]*Content {

	return deftquery.contentData
}
func (deftquery *DQuery) GetData() [][]string {
	return deftquery.data
}
func (deftquery *DQuery) GetTable() string {
	return deftquery.table
}

func (deftquery *DQuery) Select(fields ...string) *DQuery {
	deftquery.contentData = make([]map[string]*Content, len(fields))
	deftquery.fields = fields
	deftquery.queryType = QueryType_Select
	return deftquery
}

func (deftquery *DQuery) Insert(fields ...string) *DQuery {
	deftquery.contentData = make([]map[string]*Content, len(fields))
	deftquery.fields = fields
	deftquery.queryType = QueryType_Insert
	return deftquery
}
func (deftquery *DQuery) SetTable(tableName string) *DQuery {
	deftquery.table = tableName
	return deftquery
}

// func (deftquery *DQuery) SetField(fields ...string) *DQuery {
// 	deftquery.contentData = make([]map[string]*Content, len(fields))
// 	deftquery.fields = fields
// 	return deftquery
// }
func (deftquery *DQuery) Where(fields ...string) *DQuery {
	deftquery.wheres = fields
	return deftquery
}

func (deftquery *DQuery) SetValues(values ...interface{}) *DQuery {
	deftquery.values = values
	return deftquery
}
func (deftquery *DQuery) Update(fields ...string) *DQuery {
	deftquery.contentData = make([]map[string]*Content, len(fields))
	deftquery.fields = fields
	deftquery.queryType = QueryType_Update

	return deftquery
}

func getSelectFieldsString(fields []string) string {
	var field string
	if fields == nil {
		return "*"
	}
	fieldLength := len(fields)
	if fieldLength == 0 {
		return "*"
	}
	for i := 0; i < fieldLength; i++ {
		s := fields[i]
		if i == fieldLength-1 {
			field = field + s
			break
		}
		field = field + s + ","
	}
	return field
}
func getUpdateFieldsString(fields []string) string {
	var field string

	if fields == nil {
		return ""
	}
	fieldLength := len(fields)
	if fieldLength == 0 {
		return ""
	}
	field = "  SET "
	for i := 0; i < fieldLength; i++ {
		s := fields[i]
		if i == fieldLength-1 {
			field = field + s + "=?"
			break
		}
		field = field + s + "=?,"
	}
	return field
}
func getInsertFieldsString(fields []string) string {
	var field string
	if fields == nil {
		return ""
	}
	fieldLength := len(fields)
	if fieldLength == 0 {
		return ""
	}
	field = "( "
	for i := 0; i < fieldLength; i++ {
		s := fields[i]
		if i == fieldLength-1 {
			field = field + s + ")"
			break
		}
		field = field + s + ","
	}
	field = field + " VALUES("
	for i := 0; i < fieldLength; i++ {
		if i == fieldLength-1 {
			field = field + "?)"
			break
		}
		field = field + "?,"
	}
	return field
}
func getWhereFieldsString(fields []string) string {
	var field string

	if fields == nil {
		return ""
	}
	fieldLength := len(fields)
	if fieldLength == 0 {
		return ""
	}
	field = " WHERE "
	for i := 0; i < fieldLength; i++ {
		s := fields[i]
		if i == fieldLength-1 {
			field = field + s + "=?"
			break
		}
		field = field + s + "=?,"
	}
	return field
}

//		_, err1 := obj.DbMaster.Exec(`INSERT INTO auth(mailid,logoid,jersy,usertoken,country,clan,deviceid,trophy,name,coins,playerid,gems,current_data_version,account_creation_date) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)`, rm.MailId, rm.LogoId, rm.Jersy, hash, rm.Country, rm.Clan, rm.DeviceID, trophy_int, rm.Name, rm.Coins, rm.PlayerId, rm.Gems, 4, CreationTime)

func (deftquery *DQuery) Build() *DQuery {
	switch deftquery.queryType {
	case QueryType_Select:
		deftquery.query = fmt.Sprintf("SELECT " + getSelectFieldsString(deftquery.fields) + " FROM  " + deftquery.table +
			getWhereFieldsString(deftquery.wheres))
		break
	case QueryType_Update:
		deftquery.query = fmt.Sprintf("UPDATE " + deftquery.table + getUpdateFieldsString(deftquery.fields) + getWhereFieldsString(deftquery.wheres))
		break
	case QueryType_Insert:
		deftquery.query = fmt.Sprintf("INSERT INTO " + deftquery.table + getInsertFieldsString(deftquery.fields))
		break
	}
	return deftquery
}

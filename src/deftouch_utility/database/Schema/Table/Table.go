package Table

//Schema Info
const (
	Auth                  = "auth"
	Player_Info           = "player_info"
	Player_Character_Info = "player_character_info"
	Player_Stats          = "player_stats"
	UserProperties        = "userproperties"
	Friend_List           = "friend_list"
)

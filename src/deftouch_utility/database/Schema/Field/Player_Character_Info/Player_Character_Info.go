package Player_Character_Info

const (
	Rid                        = "rid"
	Playerid                   = "playerid"
	Unlocked_chars             = "unlocked_chars"
	Team_conf                  = "team_conf"
	New_unlocked_character     = "new_unlocked_character"
	Max_char_use               = "max_char_use"
	Character_purchase_history = "character_purchase_history"
	Rental_pack_info           = "rental_pack_info"
)

var AllFields []string = []string{
	"rid",
	"playerid",
	"unlocked_chars",
	"team_conf",
	"new_unlocked_character",
	"max_char_use",
	"character_purchase_history",
	"rental_pack_info",
}

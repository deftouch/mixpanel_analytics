package Analytics

const (
	Id                             = "rid"
	Email                          = "email"
	Username                       = "username"
	Acquisition_source             = "acquisition_source"
	Total_days_since_install       = "total_days_since_install"
	Total_days_since_first_install = "total_days_since_first_install"
	Total_sessions_started         = "total_sessions_started"
	Current_fb_connected           = "current_fb_connected"
	Current_xp_level               = "current_xp_level"
	Current_trophy_count           = "current_trophy_count"
	Current_arena_level            = "current_arena_level"
	Highest_trophy_count           = "highest_trophy_count"
	Highest_arena_level            = "highest_arena_level"
	Total_characters_unlocked      = "total_characters_unlocked"
	Current_resource_coin          = "current_resource_coin"
	Current_resource_gem           = "current_resource_gem"
	Current_iap_currency           = "current_iap_currency"
	Total_iap_spend                = "total_iap_spend"
	Total_iap_count                = "total_iap_count"
	Highest_purchase_price         = "highest_purchase_price"
	Avg_purchase_price             = "avg_purchase_price"
	Total_matches_started          = "total_matches_started"
	Total_matches_won              = "total_matches_won"
	Total_matches_lost             = "total_matches_lost"
	Total_chest_claimed            = "total_chest_claimed"
	Total_cards_collected          = "total_cards_collected"
	Total_cards_upgrade_events     = "total_cards_upgrade_events"
	Total_friend_matches_started   = "total_friend_matches_started"
	Total_rematches_started        = "total_rematches_started"
	Total_friends_added            = "total_friends_added"
	Current_friends_count          = "current_friends_count"
	Current_fb_friends             = "current_fb_friends"
	Deviceid                       = "deviceid"
	InstallTime                    = "installTime"
	Onboarding_skipped             = "onboarding_skipped"
)

var AllFields []string = []string{
	"rid",
	"email",
	"username",
	"acquisition_source",
	"total_days_since_install",
	"total_days_since_first_install",
	"total_sessions_started",
	"current_fb_connected",
	"current_xp_level",
	"current_trophy_count",
	"current_arena_level",
	"highest_trophy_count",
	"highest_arena_level",
	"total_characters_unlocked",
	"current_resource_coin",
	"current_resource_gem",
	"current_iap_currency",
	"total_iap_spend",
	"total_iap_count",
	"highest_purchase_price",
	"avg_purchase_price",
	"total_matches_started",
	"total_matches_won",
	"total_matches_lost",
	"total_chest_claimed",
	"total_cards_collected",
	"total_cards_upgrade_events",
	"total_friend_matches_started",
	"total_rematches_started",
	"total_friends_added",
	"current_friends_count",
	"current_fb_friends",
	"deviceid",
	"installTime",
	"onboarding_skipped",
}

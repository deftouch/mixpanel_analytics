package Player_Info

const (
	Id                           = "id"
	Country                      = "country"
	Clan                         = "clan"
	Trophy                       = "trophy"
	Name                         = "name"
	Logoid                       = "logoid"
	Coins                        = "coins"
	Chest                        = "chest"
	City                         = "city"
	Playerid                     = "playerid"
	Gems                         = "gems"
	Team                         = "team"
	Jersy                        = "jersy"
	Daily_reward                 = "daily_reward"
	Quest_info                   = "quest_info"
	Player_xp_level              = "player_xp_level"
	Current_data_version         = "current_data_version"
	Powerup                      = "powerup"
	Achivement_badge             = "achivement_badge"
	Purchasing_item_Character    = "purchasing_item_Character"
	Purchasing_item_Shoes        = "purchasing_item_Shoes"
	Purchasing_item_Emoji        = "purchasing_item_Emoji"
	Purchasing_item_Emblems      = "purchasing_item_Emblems"
	Purchasing_item_Celebration  = "purchasing_item_Celebration"
	Purchasing_item_Jersey       = "purchasing_item_Jersey"
	Previous_bind_email          = "previous_bind_email"
	Welcome_pack                 = "welcome_pack"
	Referral_code                = "referral_code"
	Referral_details             = "referral_details"
	One_signal_id                = "one_signal_id"
	Referred_by                  = "referred_by"
	Tutorial                     = "tutorial"
	Fb_id                        = "fb_id"
	Profile_picture              = "profile_picture"
	Account_creation_date        = "account_creation_date"
	Bundle_version               = "bundle_version"
	Bundle_version_code          = "bundle_version_code"
	Last_time_played             = "last_time_played"
	Chest_slot                   = "chest_slot"
	Arena_index                  = "arena_index"
	Limit_win_per_day            = "limit_win_per_day"
	Highest_unlocked_arena_index = "highest_unlocked_arena_index"
	Highest_trophy               = "highest_trophy"
	Mega_quest_claim_reward      = "mega_quest_claim_reward"
)

var AllFields []string = []string{"id",
	"country",
	"clan",
	"trophy",
	"name",
	"logoid",
	"coins",
	"chest",
	"city",
	"gems",
	"team",
	"jersy",
	"daily_reward",
	"quest_info",
	"player_xp_level",
	"current_data_version",
	"powerup",
	"achivement_badge",
	"purchasing_item_Character",
	"purchasing_item_Shoes",
	"purchasing_item_Emoji",
	"purchasing_item_Emblems",
	"purchasing_item_Celebration",
	"purchasing_item_Jersey",
	"previous_bind_email",
	"welcome_pack",
	"referral_code",
	"referral_details",
	"one_signal_id",
	"referred_by",
	"tutorial",
	"fb_id",
	"profile_picture",
	"account_creation_date",
	"bundle_version",
	"bundle_version_code",
	"last_time_played",
	"chest_slot",
	"arena_index",
	"limit_win_per_day",
	"highest_unlocked_arena_index",
	"highest_trophy",
	"mega_quest_claim_reward",
}

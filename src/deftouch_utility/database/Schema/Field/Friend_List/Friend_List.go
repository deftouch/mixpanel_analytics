package Friend_List

const (
	Id        = "id"
	User_id   = "user_id"
	Friend_id = "friend_id"
	Medium    = "medium"
)

var AllFields []string = []string{
	"id",
	"user_id",
	"friend_id",
	"medium",
}

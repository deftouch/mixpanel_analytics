package Player_Stats

const (
	Rid                  = "rid"
	Total_matches_played = "total_matches_played"
	Highest_score        = "highest_score"
	Total_runs           = "total_runs"
	Total_wickets        = "total_wickets"
	Win_streak           = "win_streak"
	Currentwinstreak     = "currentwinstreak"
	Total_ducks          = "total_ducks"
	Total_4s             = "total_4s"
	Total_6s             = "total_6s"
	Total_30s            = "total_30s"
	Total_50s            = "total_50s"
	Total_100s           = "total_100s"
	Average              = "average"
	Total_strike_rate    = "total_strike_rate"
	Total_maidens        = "total_maidens"
	Total_hattrick       = "total_hattrick"
	Total_economy_rate   = "total_economy_rate"
	Total_wins           = "total_wins"
	Total_loses          = "total_loses"
	Total_balls_faced    = "total_balls_faced"
	Total_runs_given     = "total_runs_given"
	Total_balls_bowled   = "total_balls_bowled"
)

var AllFields []string = []string{
	"rid",
	"total_matches_played",
	"highest_score",
	"total_runs",
	"total_wickets",
	"win_streak",
	"currentwinstreak",
	"total_ducks",
	"total_4s",
	"total_6s",
	"total_30s",
	"total_50s",
	"total_100s",
	"average",
	"total_strike_rate",
	"total_maidens",
	"total_hattrick",
	"total_economy_rate",
	"total_wins",
	"total_loses",
	"total_balls_faced",
	"total_runs_given",
	"total_balls_bowled",
}
